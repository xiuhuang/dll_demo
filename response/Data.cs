﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.response
{
    internal class Data
    {
        public string Code { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }

        public Data()
        {
        }

        public Data(string code, bool success, string message, object result)
        {
            this.Code = code;
            this.Success = success;
            this.Message = message;
            this.Result = result;
        }

        public static Data buildSuccess(string msg, object result)
        {
            return new Data("200", true, msg, result.ToString());
        }

        public static Data buildFailure(string msg, object result)
        {
            return new Data("201", false, msg, result.ToString());
        }
    }

}
