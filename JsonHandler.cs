﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    internal class JsonHandler
    {
        public static string ObjToJson(object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        /// <summary>
        /// 将JSON 字符串转换成对应的类型
        /// </summary>
        /// <typeparam name="T">转换后的类型</typeparam>
        /// <param name="s">需转换的字符串</param>
        /// <returns></returns>

        public static T JsonToObj<T>(string s)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(s);
            }
            catch (Exception e)
            {
                return default(T);
            }

        }
    }
}
