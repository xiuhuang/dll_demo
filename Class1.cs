﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using System.Net;
using ClassLibrary1.response;
using ClassLibrary1.request;

namespace ClassLibrary1
{
    public class Class1
    {
        public void show()
        {
            Console.WriteLine("类库ClassLibrary1.dll的show方法执行成功");
        }

        public void show2()
        {
            string url = "http://localhost:49792/api/Test";
            string t1 = HttpHandler.HttpGet(url + "?a=" + 4 + "&" + "b=" + 2);
            string t2 = HttpHandler.HttpPost(url + "?a=" + 4 + "&" + "b=" + 2,"");
            string t3 = HttpHandler.HttpPut(url + "?a=" + 4 + "&" + "b=" + 2, "");
            string t4 = HttpHandler.HttpDelete(url + "?a=" + 4 + "&" + "b=" + 2, "");

            Data d1 = JsonHandler.JsonToObj<Data>(t1);
            Data d2 = JsonHandler.JsonToObj<Data>(t2);
            Data d3 = JsonHandler.JsonToObj<Data>(t3);
            Data d4 = JsonHandler.JsonToObj<Data>(t4);

            Console.WriteLine(t1);
            Console.WriteLine(t2);
            Console.WriteLine(t3);
            Console.WriteLine(t4);

            Console.WriteLine(d1.Code + " " + d1.Success + " " + d1.Message + " " + d1.Result);
            Console.WriteLine(d2.Code + " " + d2.Success + " " + d2.Message + " " + d2.Result);
            Console.WriteLine(d3.Code + " " + d3.Success + " " + d3.Message + " " + d3.Result);
            Console.WriteLine(d4.Code + " " + d4.Success + " " + d4.Message + " " + d4.Result);
        }

        public void show3() {
            string url = "http://localhost:49792/api";
            Test test = new Test();
            test.a = 4;
            test.b = 2;
            string t1 = HttpHandler.HttpPost(url + "/Values", JsonHandler.ObjToJson(test));
            Console.WriteLine(t1);
            Data d1 = JsonHandler.JsonToObj<Data>(t1);
            Console.WriteLine(d1.Code + " " + d1.Success + " " + d1.Message + " " + d1.Result);
        }

        public void show4()
        {
            ServiceReference1.WebService1SoapClient client = new ServiceReference1.WebService1SoapClient();
            string value1 = client.HelloWorld();
            string value2 = client.GetTime("now");
            Console.WriteLine(value1);
            Console.WriteLine(value2);
        }

        public void show5()
        {
            ServiceReference1.WebService1SoapClient client = new ServiceReference1.WebService1SoapClient();
            ServiceReference1.Result value = client.GetValue(3, "三");
            foreach (ServiceReference1.Test t in value.data)
            {
                Console.WriteLine(t.ID);
                Console.WriteLine(t.Name);
            }
        }
    }

}
